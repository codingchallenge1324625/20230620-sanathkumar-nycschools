//
//  NYCSchoolInformationTests.swift
//  NYCSchoolInformationTests
//
//  Created by Sanath Kumar on 19/06/23.
//

import XCTest
@testable import NYCSchoolInformation

final class NYCSchoolInformationTests: XCTestCase {

    func testNumberOfRowsIntableView() {
        let serviceViewModel = MockSchoolServiceViewModel()
        let schoolInformation1 = SchoolInformation(schoolID: "", schoolName: "", location: "", details: "", phoneNumber: "", city: "")
        let schoolInformation2 = SchoolInformation(schoolID: "", schoolName: "", location: "", details: "", phoneNumber: "", city: "")
        serviceViewModel.assignTestData(schoolLists: [schoolInformation1, schoolInformation2])
        let schoolListViewController = SchoolListViewController()
        schoolListViewController.schoolServiceViewModel = serviceViewModel
        XCTAssertEqual(2, schoolListViewController.tableView(UITableView(), numberOfRowsInSection: 0))
    }
    
    func testFetchSchoolInformation() {
        
        let serviceViewModel = MockSchoolServiceViewModel()
        let schoolInformation1 = SchoolInformation(schoolID: "", schoolName: "", location: "", details: "", phoneNumber: "", city: "")
        let schoolInformation2 = SchoolInformation(schoolID: "", schoolName: "", location: "", details: "", phoneNumber: "", city: "")
        serviceViewModel.assignTestData(schoolLists: [schoolInformation1, schoolInformation2])

        let expectation = self.expectation(description: "fetch school information")

        serviceViewModel.mockCompletionBlock = {[weak serviceViewModel] completion in
        guard let serviceViewModel = serviceViewModel else {return}
         serviceViewModel.assignTestData(schoolLists: [schoolInformation1, schoolInformation2])
            completion(nil)
            expectation.fulfill()
        }

        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        
        guard let schoolListViewController = storyBoard.instantiateViewController(withIdentifier: "SchoolListViewController") as? SchoolListViewController else { return}
        schoolListViewController.schoolServiceViewModel = serviceViewModel
        
        schoolListViewController.loadViewIfNeeded()

        self.waitForExpectations(timeout: 0.5)

        XCTAssertEqual(2, schoolListViewController.tableView(UITableView(), numberOfRowsInSection: 0))
    }
    
}
