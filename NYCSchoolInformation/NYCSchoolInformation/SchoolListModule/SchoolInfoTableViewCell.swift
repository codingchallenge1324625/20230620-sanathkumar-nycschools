//
//  SchoolInfoTableViewCell.swift
//  NYCSchoolInformation
//
//  Created by Sanath Kumar on 19/06/23.
//

import UIKit


class SchoolInfoTableViewCell: UITableViewCell {
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var subtitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
