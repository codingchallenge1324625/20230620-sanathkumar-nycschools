//
//  SATInfoView.m
//  NYCSchoolInformation
//
//  Created by Sanath Kumar on 20/06/23.
//

#import "SATInformationView.h"

@interface SATInformationView ()

@property (weak, nonatomic) IBOutlet UILabel *satTestTakersCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *satReadingScoreLabel;
@property (weak, nonatomic) IBOutlet UILabel *satWritingScoreLabel;
@property (weak, nonatomic) IBOutlet UILabel *schoolNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *satMathScoreLabel;

@end

@implementation SATInformationView

- (void)setupSATViewWithReadingScore:(NSString *)readingScore writingScore:(NSString *)writingScore mathScore:(NSString *)mathScore testTakersCount:(NSString *)testTakersCount schoolName:(NSString *)schoolName {
    self.satReadingScoreLabel.text = readingScore;
    self.satWritingScoreLabel.text = writingScore;
    self.satMathScoreLabel.text = mathScore;
    self.satTestTakersCountLabel.text = testTakersCount;
    self.schoolNameLabel.text = schoolName;
}

- (IBAction)closeButtonTapped:(UIButton*)closeButton {
    self.didClickCloseButtonBlock();
}

@end
