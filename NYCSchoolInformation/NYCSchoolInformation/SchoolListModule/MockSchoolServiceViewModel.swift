//
//  MockSchoolServiceViewModel.swift
//  NYCSchoolInformation
//
//  Created by Sanath Kumar on 20/06/23.
//

import Foundation

typealias MockCompletionBlock = ((ViewModelCompletion) -> ())
class MockSchoolServiceViewModel: SchoolServiceViewModelProtocol {
    
    func getSelectedSchoolInformation(schoolListInformation: SchoolInformation) -> SchoolSATInformation? {
        return nil
    }
    
    var schoolLists:[SchoolInformation] = []
    var mockCompletionBlock: MockCompletionBlock?
    
    func assignTestData(schoolLists:[SchoolInformation]){
        self.schoolLists = schoolLists
    }
    
    func fetchCompleteSchoolsInformation(completion: @escaping ViewModelCompletion) {
        mockCompletionBlock?(completion)
    }
    
}
