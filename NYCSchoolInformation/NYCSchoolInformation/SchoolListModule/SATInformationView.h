//
//  SATInfoView.h
//  NYCSchoolInformation
//
//  Created by Sanath Kumar on 20/06/23.
//

//Objective-C Class
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void (^DidClickCloseButtonBlock)(void);
@interface SATInformationView : UIView
@property (copy, nonatomic) DidClickCloseButtonBlock didClickCloseButtonBlock;
- (void)setupSATViewWithReadingScore:(NSString *)readingScore writingScore:(NSString *)writingScore mathScore:(NSString *)mathScore testTakersCount:(NSString *)testTakersCount schoolName:(NSString *)schoolName;
@end

NS_ASSUME_NONNULL_END
