//
//  ViewController.swift
//  NYCSchoolInformation
//
//  Created by Sanath Kumar on 19/06/23.
//

import UIKit


class SchoolListViewController: UIViewController {
    
    @IBOutlet weak var schoolListTableView: UITableView!{
        didSet{
            setupTableView()
        }
    }
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    var schoolServiceViewModel: SchoolServiceViewModelProtocol = SchoolServiceViewModel()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "NYC Schools"
        fetchSchoolInformation()
    }
    
    func addSpinnerView() {
        activityIndicatorView.startAnimating()
        activityIndicatorView.isHidden = false
    }
    
    func removeSpinnerView() {
        activityIndicatorView.startAnimating()
        activityIndicatorView.isHidden = true
    }
    
    func fetchSchoolInformation() {
        self.addSpinnerView()
        self.schoolServiceViewModel.fetchCompleteSchoolsInformation {[weak self] error in
            guard let self = self else {return}
            self.removeSpinnerView()
            if let error = error {
                self.showAlertForMissingSchoolSatInformation(message: error.localizedDescription)
            }
            else {
                self.schoolListTableView.reloadData()
            }
        }
    }
    
    func setupTableView() {
        self.schoolListTableView.delegate = self
        self.schoolListTableView.dataSource = self
        self.schoolListTableView.estimatedRowHeight = 80
        self.schoolListTableView.register(UINib(nibName: "SchoolInfoTableViewCell", bundle: nil), forCellReuseIdentifier: "SchoolInfoTableViewCell")
    }
}


extension SchoolListViewController:UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.schoolServiceViewModel.schoolLists.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let schoolInfoTableViewCell: SchoolInfoTableViewCell = tableView.dequeueReusableCell(withIdentifier: "SchoolInfoTableViewCell", for: indexPath) as? SchoolInfoTableViewCell else { return UITableViewCell()
        }
        let schoolInformation = self.schoolServiceViewModel.schoolLists[indexPath.row]
        schoolInfoTableViewCell.title.text = schoolInformation.schoolName
        schoolInfoTableViewCell.subtitle.text = schoolInformation.city
        
        return schoolInfoTableViewCell
    }
}

extension SchoolListViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let selectedSchoolInformation = self.schoolServiceViewModel.getSelectedSchoolInformation(schoolListInformation: self.schoolServiceViewModel.schoolLists[indexPath.row]) else {
            self.showAlertForMissingSchoolSatInformation(message: "SAT information is not available for the selected school")
            return
        }
        let satInformationView: SATInformationView = Bundle.main.loadNibNamed("SATInformationView", owner: self)?.first as! SATInformationView
        satInformationView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        let transparentView = UIView(frame: CGRect.zero)
        transparentView.backgroundColor = UIColor.gray.withAlphaComponent(0.7)
        transparentView.frame = self.view.frame
        
        satInformationView.setupSATView(withReadingScore: selectedSchoolInformation.satCriticalReadingAvgStore, writingScore: selectedSchoolInformation.satWritingAvgScore, mathScore: selectedSchoolInformation.satMathAvgScore, testTakersCount: selectedSchoolInformation.testTakersCount, schoolName: selectedSchoolInformation.schoolName)

        
        satInformationView.didClickCloseButtonBlock = {[weak satInformationView] in
            guard let satInformationView = satInformationView else {return}
            satInformationView.removeFromSuperview()
            transparentView.removeFromSuperview()
        }
        
        self.view.addSubview(transparentView)
        self.view.addSubview(satInformationView)
    }
    
    func showAlertForMissingSchoolSatInformation(message: String) {
        let alertController = UIAlertController(title: "Information Unavailable", message: message, preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "OK", style: .default)
        alertController.addAction(alertAction)
        self.present(alertController, animated: true)
    }
    
}

