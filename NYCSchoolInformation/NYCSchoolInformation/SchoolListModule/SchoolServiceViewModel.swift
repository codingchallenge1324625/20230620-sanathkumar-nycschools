//
//  SchoolServiceViewModel.swift
//  NYCSchoolInformation
//
//  Created by Sanath Kumar on 19/06/23.
//

import Foundation

struct SchoolInformation: Codable {
    let schoolID: String
    let schoolName:String
    let location: String
    let details: String
    let phoneNumber: String
    let city: String
    
    enum CodingKeys: String, CodingKey {
        case schoolID = "dbn"
        case schoolName = "school_name"
        case location = "location"
        case details = "overview_paragraph"
        case phoneNumber = "phone_number"
        case city = "city"
    }
}

struct SchoolSATInformation: Codable {
    let schoolID: String
    let schoolName:String
    let testTakersCount:String
    let satCriticalReadingAvgStore:String
    let satMathAvgScore:String
    let satWritingAvgScore:String
    
    enum CodingKeys: String, CodingKey {
        case schoolID = "dbn"
        case schoolName = "school_name"
        case testTakersCount = "num_of_sat_test_takers"
        case satCriticalReadingAvgStore = "sat_critical_reading_avg_score"
        case satMathAvgScore = "sat_math_avg_score"
        case satWritingAvgScore = "sat_writing_avg_score"
    }
}

typealias ViewModelCompletion = ((Error?) -> ())

protocol SchoolServiceViewModelProtocol {
    var schoolLists:[SchoolInformation] {get set}
    func fetchCompleteSchoolsInformation(completion: @escaping ViewModelCompletion)
    func getSelectedSchoolInformation(schoolListInformation: SchoolInformation) -> SchoolSATInformation?
}

class SchoolServiceViewModel: SchoolServiceViewModelProtocol {
    var schoolLists:[SchoolInformation] = []
    var schoolsSatInformation:[SchoolSATInformation] = []
    
    private func fetchSchoolList(completion: @escaping ViewModelCompletion) {
        SchoolsDetailsService.fetchListofSchools.request { [weak self] result in
            guard let self = self else {
                return
            }
            switch result {
            case .success(let responseData):
                do {
                    self.schoolLists = try JSONDecoder().decode([SchoolInformation].self, from: responseData)
                    completion(nil)
                }
                catch {
                    completion(error)
                }
            case .failure(let error):
                completion(error)
            }
        }
    }
    
    private func fetchSchoolInformation(completion: @escaping ViewModelCompletion){
        SchoolsDetailsService.fetchSchoolInformation.request { [weak self] result in
            guard let self = self else {
                return
            }
            switch result {
            case .success(let responseData):
                do {
                    self.schoolsSatInformation = try JSONDecoder().decode([SchoolSATInformation].self, from: responseData)
                    completion(nil)
                }
                catch {
                    completion(error)
                }
            case .failure(let error):
                completion(error)
            }
        }
    }
    
    func fetchCompleteSchoolsInformation(completion: @escaping ViewModelCompletion){
        var dataError: Error?
        let dispatchGroup = DispatchGroup()
        dispatchGroup.enter()
        
        fetchSchoolList { error in
            dataError = error
            dispatchGroup.leave()
        }
        
        dispatchGroup.enter()
        
        fetchSchoolInformation { error in
            dataError = error
            dispatchGroup.leave()
        }
        
        dispatchGroup.notify(queue: .main) {
            completion(dataError)
        }
    }
    
    func getSelectedSchoolInformation(schoolListInformation: SchoolInformation) -> SchoolSATInformation?{
        self.schoolsSatInformation.first { schoolInformation in
            schoolListInformation.schoolID == schoolInformation.schoolID
        }
    }
}

