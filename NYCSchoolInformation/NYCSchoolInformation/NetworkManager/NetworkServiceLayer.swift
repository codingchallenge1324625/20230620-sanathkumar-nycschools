//
//  NetworkServiceLayer.swift
//  NYCSchoolInformation
//
//  Created by Sanath Kumar on 19/06/23.
//

import Foundation


/// Network error type for handling all kinds of errors
///
/// - badRequest: bad request if the request is not in the correct format
/// - notFound: url not found
/// - internalServerError: server error
/// - otherError: decoding/ encoding failed, status == 200 but data not found
enum NetworkErrorType: Error {
    case badRequest(statusCode: Int)
    case notFound(statusCode: Int)
    case internalServerError(statusCode: Int)
    case otherError(statusCode: Int)
    
    var code: Int {
        var errorCode: Int = 0
        
        switch self {
        case .badRequest(let statusCode):
            errorCode = statusCode
        case .notFound(let statusCode):
            errorCode = statusCode
        case .internalServerError(let statusCode):
            errorCode = statusCode
        case .otherError(let statusCode):
            errorCode = statusCode
        }
        
        return errorCode
    }
    
    var error: NSError {
        return NSError(domain: "othererror", code: code, userInfo: [ NSLocalizedDescriptionKey: "we were unable to process your requests" ] )
    }
}

/// Service layer
final class ServiceLayer {
    
    static func request(request: URLRequest, completion: @escaping (Result<Data, Error>) -> ()) {
        
        guard let session = SessionHandler.shared.session else {
            return
        }
        let dataTask = session.dataTask(with: request) { (data, response, error) in
            guard  error == nil else {
                if let error = error {
                    Self.invokeCompletionOnMainThread(result: .failure(error), completion: completion)
                }
                return
            }
            
            
            guard let httpResponse = response as? HTTPURLResponse else {
                Self.invokeCompletionOnMainThread(result: .failure(NetworkErrorType.otherError(statusCode: 0)), completion: completion)
                return
            }
            
            guard let data = data else {
                Self.invokeCompletionOnMainThread(result: .failure(NetworkErrorType.otherError(statusCode: 0)), completion: completion)
                return
            }
            // all the error codes in 200 series
            if 200 ... 299 ~= httpResponse.statusCode {
                Self.invokeCompletionOnMainThread(result: .success(data), completion: completion)
            } else {
                var error = NetworkErrorType.otherError(statusCode: 0)
                switch httpResponse.statusCode {
                case 400:
                    error = NetworkErrorType.badRequest(statusCode: 400)
                case 404:
                    error = NetworkErrorType.notFound(statusCode: 404)
                case 500:
                    error = NetworkErrorType.internalServerError(statusCode: 500)
                default:
                    break
                }
                Self.invokeCompletionOnMainThread(result: .failure(error.error), completion: completion)
            }
        }
        
        dataTask.resume()
    }
    
    
    class func invokeCompletionOnMainThread(result: (Result<Data, Error>), completion: @escaping (Result<Data, Error>) -> ()) {
        DispatchQueue.main.async {
            completion(result)
        }
    }
    
    
    /// Get url request from the API resource passed
    ///
    /// - Parameter resource: resource
    /// - Returns: returns urlRequest
    class func getUrlRequest(resource: APIResource) -> URLRequest? {
        guard let url = resource.url else {
            return nil
        }
        
        var urlRequest = URLRequest(url: url)
        
        urlRequest.httpMethod = resource.method.rawValue
        
        urlRequest.httpBody = resource.encodedData
        
        resource.headers.forEach({ (arg) in
            
            let (key, value) = arg
            urlRequest.setValue(value, forHTTPHeaderField: key)
        })
        
        return urlRequest
    }
}


final class SessionHandler: NSObject {
    static let shared = SessionHandler()
    
    var session: URLSession!
    
    private override init() {
        super.init()
        session = URLSession(configuration: .default)
    }
}


