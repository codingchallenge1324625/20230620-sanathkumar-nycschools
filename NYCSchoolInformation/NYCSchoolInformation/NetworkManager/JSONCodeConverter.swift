//
//  JSONCodeConverter.swift
//  NYCSchoolInformation
//
//  Created by Sanath Kumar on 19/06/23.
//

import Foundation

final class JSONConverter {
    /// get json decoded data
    ///
    /// - Parameters:
    ///   - response: api response
    ///   - protocolData: protocol data model
    /// - Returns: data model
    static func getDecodedData<T: Decodable>(response: Data, protocolData: T.Type) -> [T]? {
        do {
            let json = try JSONDecoder().decode([T].self, from: response)
            return json
        } catch {
#if DEBUG
            print(error.localizedDescription)
#endif
        }
        return nil
    }
    
    static func getEncodeDataFrom<T: Encodable>(data: T) -> Data? {
        let jsonEncoder = JSONEncoder()
        do {
            let jsonData = try jsonEncoder.encode(data)
            print()
            return jsonData
        } catch {
#if DEBUG
            print(error.localizedDescription)
#endif
        }
        return nil
    }
}
