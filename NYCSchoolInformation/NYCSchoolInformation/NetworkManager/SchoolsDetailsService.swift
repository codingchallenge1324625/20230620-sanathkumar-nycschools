//
//  SchoolsDetailsService.swift
//  NYCSchoolInformation
//
//  Created by Sanath Kumar on 19/06/23.
//

import Foundation

enum SchoolsDetailsService: APIService {
    case fetchListofSchools
    case fetchSchoolInformation
    
    var method: NetworkManagerMethod {
        return .Get
    }
    
    var command: String {
        switch self {
        case .fetchListofSchools:
            return "/resource/s3k6-pzi2.json"
        case .fetchSchoolInformation:
            return "/resource/f9bf-2cp4.json"
        }
    }
}
