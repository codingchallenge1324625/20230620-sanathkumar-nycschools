//
//  BaseAPIService.swift
//  NYCSchoolInformation
//
//  Created by Sanath Kumar on 19/06/23.
//

import Foundation


enum NetworkManagerMethod: String {
    case Get = "Get"
    case Post = "Post"
    case Put = "Put"
}


struct APIResource {
    var url: URL?
    
    var encodedData: Data?
    
    var method: NetworkManagerMethod = .Get
    
    var priority = URLSessionTask.defaultPriority
    
    var headers: [String: String] = [ "Content-Type": "application/json", "Accept-Type": "application/json" ]
}


protocol APIService {
    //method
    var method: NetworkManagerMethod {get}
    
    //scheme
    var scheme: String {get}
    
    //baseurl
    var baseURL: String {get}
    
    //command
    var command: String {get}
    
    //prepare
    func prepareResource(_ resource: inout APIResource)
    
    //request
    func request(completion: @escaping (Result<Data, Error>) -> ())
}


extension APIService {
    var method: NetworkManagerMethod {
        return .Get
    }
    
    var scheme: String {
        return "https"
    }
    
    var baseURL:  String {
        return "data.cityofnewyork.us"
    }
    
    func prepareResource(_ resource: inout APIResource) {}
    
    func resource() -> APIResource  {
        var resource = APIResource()
        
        var urlcomponents = URLComponents()
        urlcomponents.scheme = scheme
        urlcomponents.host = baseURL
        urlcomponents.path = command
        
        guard let url = urlcomponents.url else {
            return APIResource()
        }
        
        resource.url = url
        resource.method = method
        prepareResource(&resource)
        
        return resource
    }
    
    func URLRequest() -> URLRequest? {
        return ServiceLayer.getUrlRequest(resource: resource())
    }
    
    func request(completion: @escaping (Result<Data, Error>) -> ()) {
        guard let request = URLRequest() else {
            // appropriate error after completion
            return
        }
        ServiceLayer.request(request: request, completion: completion)
    }
    
}
